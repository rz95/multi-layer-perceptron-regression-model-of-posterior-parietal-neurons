# Class MLPRegressor implements a multi-layer perceptron (MLP) that trains using backpropagation with no activation function in the output layer, 
# which can also be seen as using the identity function as activation function. 
# Therefore, it uses the square error as the loss function, and the output is a set of continuous values.
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
import xlrd
import numpy as np

#local path
loc1 = ("/Users/edna/Desktop/BackProp/gdist.xls")
wb1 = xlrd.open_workbook(loc1)
sh1 = wb1.sheet_by_index(0)

loc2 = ("/Users/edna/Desktop/BackProp/ldist.xls")
wb2 = xlrd.open_workbook(loc2)
sh2 = wb2.sheet_by_index(0)

RPs = []
for i in range(1, sh1.nrows):
    RPs.append(sh1.cell_value(i,0))

# RP is the relative position of the datapoint to the mean
def getfr(RP):
    for i in range(len(RPs)):
        if(abs(RPs[i]-RP)<1e-6):
            return sh1.cell_value(i+1,1)
        if(i==len(RPs)-1):
            return 0

# Combo gives TH when input with TE and EH
# and gives a list of 10 neurons' firing rate when input with 5 TE neurons and 5 EH neurons
def combo(lst1,lst2):
    ret = []
    for e1 in lst1:
        for e2 in lst2:
            ret.append(e1+e2)
    return ret

# Variables named with 1 simulate TE neurons with gaussian fr, variables named with 2 simulate EH neurons with linear fr
# Training, y_train1 is the same as y_train2
ct = [-5, -2.5, 0, 2.5, 5]
y_train1 = [-5, -3.75, -2.5, -1.25, 0, 1.25, 2.5, 3.75, 5]
x_train1 = []
for TE in y_train1:
    frs = []
    for c in ct:
        frs.append(getfr(TE-c))
    x_train1.append(frs)

y_train2 = y_train1
x_train2 = []
for i in range (1,10):
    frs = []
    for j in range (sh2.ncols-1):
        frs.append(sh2.cell_value(i,j+1))
    x_train2.append(frs)

y_train = combo(y_train1, y_train2)
x_train = combo(x_train1, x_train2)

#print(x_train)
#print(y_train)

# Testing 
y_test1 = [-4.5, -3.2, -1.9, -0.6, 0.7, 2, 3.3, 4.6]
x_test1 = []
for TE in y_test1:
    frs = []
    for c in ct:
        frs.append(getfr(TE-c))
    x_test1.append(frs)

y_test2 = y_test1
x_test2 = []
for i in range (10,18):
    frs = []
    for j in range (sh2.ncols-1):
        frs.append(sh2.cell_value(i,j+1))
    x_test2.append(frs)

y_test = combo(y_test1, y_test1)
x_test = combo(x_test1, x_test2)

#print(x_test)
#print(y_test)

# Solver can be {‘lbfgs’, ‘sgd’, ‘adam’}
# Starts to asymptote at around 1000 iterations
regr = MLPRegressor(solver='adam', activation='logistic', alpha=1e-5, hidden_layer_sizes=(32), random_state=42, max_iter = 5000)
regr.fit(x_train, y_train)
prediction = regr.predict(x_test)

def RELU(x):
    if x>0:
        return x
    else:
        return 0

# Hidden layer output applied with RELU
hid = (np.matmul(x_test, regr.coefs_[0]) + regr.intercepts_[0])
for i in range(len(hid)):
    for j in range (len(hid[i])):
        hid[i][j] = RELU(hid[i][j])

# peak[0] to peak[7] correspond respectively to maximum hidden neuron firing rate when TE=-4.5, -3.2, -1.9, -0.6, 0.7, 2, 3.3, 4.6 while EH is changing
peak = []
for i in range(0,8):
    targ = []
    for j in range(8*i, 8*i+8):
        targ.append(max(hid[j]))
    peak.append(targ)

plt.figure()
plt.plot(y_test1, peak[0], color="blue", label='TE=-4.5')
plt.plot(y_test1, peak[1], color="red", label='TE=-3.2')
plt.plot(y_test1, peak[2], color="green", label='TE=-1.9')
plt.plot(y_test1, peak[3], color="yellow", label='TE=-0.6')
plt.plot(y_test1, peak[4], color="purple", label='TE=0.7')
plt.plot(y_test1, peak[5], color="pink", label='TE=2')
plt.plot(y_test1, peak[6], color="orange", label='TE=3.3')
plt.plot(y_test1, peak[7], color="brown", label='TE=4.6')
plt.xlabel("EH")
plt.ylabel("Firing Rate")
plt.legend()
plt.show()

# Get all the indices in hidden layer that correspond to the same EH with different TEs
def all(init):
    ind = []
    for k in range(0,8):
        ind.append(init+k*8)
    return ind

# rcp[0] to rcp[7] is respectively for EH=-4.5, -3.2, -1.9, -0.6, 0.7, 2, 3.3, 4.6 while TE is changing
# Different neurons' retinal receptive field
rcp=[]
for m in range(8): #every EH
    eye = []
    for n in range(32): #every neuron
        nrn = []
        for elem in all(m): #with different TEs
            nrn.append(hid[elem][n])
        eye.append(nrn)
    rcp.append(eye)

# One plot per EH per neuron
# Save to local location
for i in range(len(rcp)):
    for j in range(len(rcp[i])):
        lb1 = str(y_test1[i])
        lb2 = str(j+1)
        plt.figure()
        plt.plot(rcp[i][j], label='EH='+lb1+' '+'Neuron No.'+lb2)
        plt.legend()
        #plt.show()
        plt.savefig("/Users/edna/Desktop/BackProp/hidden neuron outputs/"+"("+lb1+","+lb2+").png")

#plot prediction v.s. TH
plt.figure()
plt.plot(y_test, prediction, color="blue")
plt.xlabel("TH")
plt.ylabel("Prediction")
plt.show()

